import uuid, math, urllib3, io
import numpy as np
import csv, requests
import pandas as pd
import json
from time import sleep
from astropy.coordinates import SkyCoord
from astropy.io import fits
from astropy import wcs
from django.db import models
from django.utils import timezone
from django.conf import settings
from django.db.models import Max, Min, F
from statistics import robust_stats_radio, ra_increment, get_quadrangle_from_quad

settings.configure()

EDGE_RA_TILES = ['T01t01', 'T01t48', 'T02t01', 'T02t48', 'T03t01',
                'T03t36', 'T04t01', 'T04t36', 'T05t01', 'T05t36',
                'T06t01', 'T06t36', 'T07t01', 'T07t36', 'T08t01',
                'T08t36', 'T09t01', 'T09t36', 'T10t01', 'T10t36',
                'T11t01', 'T11t36', 'T12t01', 'T12t36', 'T13t01',
                'T13t01', 'T14t01', 'T14t36', 'T15t01', 'T15t32',
                'T16t01', 'T16t32', 'T17t01', 'T17t32', 'T18t01',
                'T18t32', 'T19t01', 'T19t30', 'T20t01', 'T20t30',
                'T21t01', 'T21t24', 'T22t01', 'T22t24', 'T23t01',
                'T23t24', 'T24t01', 'T24t24', 'T25t01', 'T25t18',
                'T26t01', 'T26t18', 'T27t01', 'T27t15', 'T28t01',
                'T28t12', 'T29t01', 'T29t09', 'T30t01', 'T30t09',
                'T31t01', 'T31t04', 'T32t01', 'T32t02']

EDGE_DEC_TILES = ['T32t01', 'T32t02'] #max 90

class VLASSQl(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    subtile = models.CharField(max_length=18)
    url = models.URLField(unique=True)
    created_on = models.DateTimeField(auto_now_add=True)
    # from header or filenameID
    epoch = models.FloatField() # FILNAM01= 'VLASS1  and FILNAM02= '1
    tilename = models.CharField(max_length=8) # FILNAM04= 'T10t32  '
    version = models.CharField(max_length=2) # FILNAM08= 'v1      '
    stokes = models.CharField(max_length=1) # FILNAM09= 'I       '
    obs_date = models.DateTimeField() #header['Date-OBS']
    #calculated from robust_stats_radio
    FluxRMS = models.FloatField()
    FluxSTD = models.FloatField()
    FluxMin = models.FloatField()
    FluxMax = models.FloatField()
    FluxMean = models.FloatField()
    FluxMedian = models.FloatField()
    # extras from manitoba may not have for all
    Location_VOSpace = models.CharField(max_length = 150, null=True, blank=True, default=None)
    Mean_isl_rms = models.FloatField(null=True, blank=True, default=None)# [mJy/beam]
    SD_isl_rms = models.FloatField(null=True, blank=True, default=None)# [mJy/beam]
    Peak_flux_p25 = models.FloatField(null=True, blank=True, default=None)# [mJy/beam]
    Peak_flux_p50 = models.FloatField(null=True, blank=True, default=None)# [mJy/beam]
    Peak_flux_p75 = models.FloatField(null=True, blank=True, default=None)# [mJy/beam]
    Peak_flux_max = models.FloatField(null=True, blank=True, default=None)# [mJy/beam]
    N_pybdsf_components = models.IntegerField(null=True, blank=True, default=None) # originally found from pybdsf
    N_empty_islands = models.IntegerField(null=True, blank=True, default=None)
    BMAJ = models.FloatField()# in degrees!
    BMIN = models.FloatField()# stored from header
    BPA = models.FloatField()

    class Meta:
        constraints = [
            models.UniqueConstraint(fields=['subtile', 'epoch'], name='unique subtile per epoch')
        ]

    # @classmethod
    # def create(header, image_data, Link_CADCArchive, Mean_isl_rms=None, SD_isl_rms=None,
    #             Peak_flux_p25=None, Peak_flux_p50=None, Peak_flux_p75=None, Peak_flux_max=None,
    #             N_components=None, N_empty_islands=None):
    @classmethod
    def create(cls, header, image_data, Link_CADCArchive, optional_args={}):
        epoch = header['FILNAM01'][-1] +'.'+ header['FILNAM02']
        ###nameparts SHOULD HAVE LENGTH 12?????? todo ASSERT THIS IS TRUE
        tilename = header['FILNAM04']
        subtile = header['FILNAM05']
        version = header['FILNAM08']
        stokes = header['FILNAM09']
        obs_date = header['Date-OBS']
        BMAJ = header['BMAJ']
        BMIN = header['BMIN']
        BPA = header['BPA']

        #use built in: probs too        hdul = fits.open(filename) much computation
        # w = WCS(hdu[0], naxis=2)
        # w.pixel_shape = w.pixel_shape[0:2]
        # w.footprint_contains(c)
        # import from CORE code
        (FluxMin, FluxMax, FluxMedian, FluxRMS, FluxSTD, FluxMean) = robust_stats_radio(image_data)
        new_ql = cls(url=Link_CADCArchive, epoch=epoch, tilename=tilename, subtile=subtile,
                    version=version, stokes=stokes, obs_date=obs_date, BMAJ=BMAJ, BMIN=BMIN, BPA=BPA,
                    FluxMin=FluxMin, FluxMax=FluxMax,FluxMean=FluxMean,
                    FluxMedian=FluxMedian, FluxRMS=FluxRMS, FluxSTD=FluxSTD,
                    **optional_args)

        return new_ql

    # @staticmethod
    # def get_fits_data(url):
    #     fits_file = io.BytesIO(send_request(url))
    #     fits_file.seek(0)
    #     hdul = fits.open(fits_file, ignore_missing_end= True)
    #     header = hdul[0].header
    #     data = hdul[0].data
    #     fits_file.close()
    #     return header, data

    @staticmethod
    def ql_minmax_indices(data):
    # gets real boundaries and skips over all the NANs
    # Assumes x,y, nu, stokes stored in reverse order
        # 1 indexed instead of 0... but actually 0.5 min and 3722.5 max
        valid_data = np.argwhere(np.isfinite(data[0,0,:,:]))+1
        valid_x = valid_data[:,1]
        valid_y = valid_data[:,0]
        return(valid_x.min()-0.5,valid_x.max()+0.5,valid_y.min()-0.5,valid_y.max()+0.5)

    def __str__(self):
        return f"{self.subtile}, {self.tilename}, epoch{self.epoch}"

#created when a VLASS_QL item is created
# describes the region bounded within the QL
class RegionQl(models.Model):
    ql = models.OneToOneField(VLASSQl, on_delete=models.CASCADE)
    # header vals for region
    RA_pixelsize = models.FloatField() #abs(CDELT1) X values
    DEC_pixelsize = models.FloatField() #abs(CDELT2) Y values
    RA_size = models.IntegerField() #NAXIS1
    DEC_size = models.IntegerField() #NAXIS2
    RA_center = models.FloatField() # header[CRVAL1]
    DEC_center = models.FloatField() # header[CRVAL2]
    RA_min = models.FloatField()
    DEC_min = models.FloatField()
    RA_max = models.FloatField()
    DEC_max = models.FloatField()
    center_X = models.FloatField() #CRPIX1
    center_Y = models.FloatField() #CRPIX2
    valid_Xmin = models.FloatField()
    valid_Xmax = models.FloatField()
    valid_Ymin = models.FloatField()
    valid_Ymax = models.FloatField()

    credentials = json.load("secrets.json")
    connection = pymysql.connect(
        host=credentials["host"],
        user=credentials["user"],
        password=credentials["password"],
        database=credentials["database"]
    )

    '''
    NOTE: Code like
        my_new_instance = MyModel.objects.create(my_field='my_field value')
    will execute this modified create method, but code like:
        my_new_unsaved_instance = MyModel(my_field='my_field value')
    will not

    '''

    @classmethod
    def create(cls, header, image_data, ql):
        # get minmax edged and update here and change size depen
        args = {'ql':ql}
        (args['valid_Xmin'], args['valid_Xmax'], args['valid_Ymin'], args['valid_Ymax']) = VLASSQl.ql_minmax_indices(image_data)
        args['RA_pixelsize'] = header['CDELT1'] # CDELT1 is always < 0
        args['DEC_pixelsize'] = header['CDELT2']
        args['RA_size'] = header['NAXIS1']
        args['DEC_size'] = header['NAXIS2']
        args['RA_center'] = header['CRVAL1']%360
        args['DEC_center'] = header['CRVAL2']#/cos(DEC)
        args['center_X'] = header['CRPIX1']
        args['center_Y'] = header['CRPIX2']

        real_DECmax = args['DEC_size']+0.5
        if args['valid_Ymax']<real_DECmax:
            real_DECmax = args['valid_Ymax']
        real_DECmin = 0.5
        if args['valid_Ymin']>real_DECmin:
            real_DECmin=args['valid_Ymin']
        real_RAmin = 0.5
        if args['valid_Xmin']>real_RAmin:
            real_RAmin = args['valid_Xmin']
        real_RAmax = args['RA_size']+0.5
        if args['valid_Xmax']<real_RAmax:
            real_RAmax = args['valid_Xmax']
        # # TODO: check if CTYPE1 and CTYPE2 are deg otherwise convert first
        args['DEC_max'] = min(args['DEC_center']+((real_DECmax-header['CRPIX2'])*args['DEC_pixelsize']),90)
        args['DEC_min'] = max(args['DEC_center']-((header['CRPIX2']-real_DECmin)*args['DEC_pixelsize']),-90)
        if args['DEC_min'] > args['DEC_max']: #went over the pole
            args['DEC_min'], args['DEC_max'] = args['DEC_max'], args['DEC_min']
        ra_min_inc = (real_RAmax-header['CRPIX1'])*args['RA_pixelsize']
        ra_max_inc = (header['CRPIX1']-real_RAmin)*args['RA_pixelsize']
        #old way
        # args['RA_min'] = (args['RA_center']+ra_increment(ra_min_inc, args['DEC_min'], args['DEC_max']))%360
        # args['RA_max'] = (args['RA_center']-ra_increment(ra_max_inc, args['DEC_min'], args['DEC_max']))%360
        #Ra is skewed by a factor of DEC
        min_inc = ra_increment(ra_min_inc, args['DEC_min'], args['DEC_max'])
        if abs(min_inc) >=180:
            args['RA_min'] = 0.0
        else:
            args['RA_min'] = (args['RA_center']+min_inc)%360
        max_inc = ra_increment(ra_max_inc, args['DEC_min'], args['DEC_max'])
        if abs(max_inc) >=180:
            args['RA_max'] = 360.0
        else:
            args['RA_max'] = (args['RA_center']-max_inc)%360
        new_region = cls(**args)
        return new_region

    # the RA_min of choice is actually the one that wins Min((each_RAmin+180)%360).
    # RA_max would also be the one that wins Max((each_RA_max-180)%360)
    @classmethod
    def get_combined_boundary(self, cls, region_ids, tilename):
        '''
        This is for combined regions within a Tile to look for component sets that disappeared
        '''
        if tilename in EDGE_RA_TILES:
            # TODO PyMySQL
            # best_q1 = cls.objects.filter(id__in=region_ids)
            with self.connection:
                with self.connection.cursor() as cursor:
                    sql = ""
                    cursor.execute(sql)
                    best_q1 = cursor.fetchall_unbuffered()


            best_all = best_q1.aggregate(dec_max=Max('DEC_max'), dec_min=Min('DEC_min'))
            all_ra = best_q1.values(ra_min=F('RA_min'), ra_max=F('RA_max'))
            # [{RA_min:0, RA_max:0}, {RA_min:0, RA_max:0}, {RA_min:0, RA_max:0}, ...]
            items = {}
            min_vals = []
            max_vals = []
            for i, ra in enumerate(all_ra):
                items[i] = {'ra_min':ra['ra_min'], 'ra_max':ra['ra_max']}
                min_vals.append((ra['ra_min']+180)%360) # = {(r['ra_min']+180)%360:r['ra_min'] for r in all_ra}
                max_vals.append((ra['ra_max']-180)%360)
            best_all['ra_min'] = items[min_vals.index(min([k for k in min_vals]))]['ra_min']
            best_all['ra_max'] = items[max_vals.index(max([k for k in max_vals]))]['ra_max']
            if tilename in EDGE_DEC_TILES:
                best_all['dec_max']= max(best_all['dec_max'],90)
        else:
            # TODO PyMySQL
            best_all = cls.objects.filter(id__in=region_ids).aggregate(dec_max=Max('DEC_max'), dec_min=Min('DEC_min'), ra_min=Min('RA_min'), ra_max=Min('RA_max'))
        return best_all

    @classmethod
    # TODO PyMySQL
    def get_regions_per_tile(cls, tilename, epoch=None):
        if epoch:
            return cls.objects.filter(ql__tilename=tilename, ql__epoch=epoch)
        else:
            return cls.objects.filter(ql__tilename=tilename)

    # def get_quadrilateral(self):
    #     return {'RA_min' : self.RA_min, 'RA_max' : self.RA_max,
    #             'DEC_min' : self.DEC_min, 'DEC_max' : self.DEC_max}



    # https://stackoverflow.com/questions/32425847/how-to-filter-a-queryset-using-a-function-in-django
    # First, define a manager subclass???????????
    def includes(self, ra,dec):
        '''
        returns true if ra,dec position in degrees within ql Region boundaries
        python does the work instead of DB....
        '''
        # ra_inc = ra_increment(settings.BOUNDARY_ERROR, dec)
        (ra_min,ra_max,dec_min, dec_max) = get_quadrangle_from_quad(self.RA_min,self.RA_max,self.DEC_min, self.DEC_max, settings.BOUNDARY_ERROR)
        dec_includes = dec_min<=dec<=dec_max
        if ra_min>ra_max:
            ra_includes = ra_min<=ra<=360.0 or 0<=ra<=ra_max
        else:
            ra_includes = ra_min<=ra<=ra_max
        return (ra_includes and dec_includes)

    # calculated x,y coords for RA,DEC position in a QL
    def calculate_x_y(self,RA,DEC=None):
        if not DEC:
            DEC = RA['DEC']
            RA = RA['RA']
        w = wcs.WCS(naxis=2)
        w.wcs.crval=[self.RA_center,self.DEC_center]
        w.wcs.ctype=["RA---SIN", "DEC--SIN"]
        w.wcs.crpix=[self.center_X,self.center_Y]
        w._naxis = [self.RA_size, self.DEC_size]
        w.wcs.cdelt=np.array([self.RA_pixelsize,self.DEC_pixelsize])
        coords = SkyCoord(RA,DEC, unit="deg")
        x,y = coords.to_pixel(w,1)
        return [float(x),float(y)]


# just helper class not real manager
"""
1. download ql (each computer handles 1 tile)
2. pybdsf + classification
3. ingestion (dont paralellize)
4. generate ql images
5. ingestion (dont parallelize)
"""
class QlManager():
    # create ql then region from image data and url
    @classmethod
    def create_ql_with_region(cls, header, image_data, Link_CADCArchive, optional_args={}):
        try:
            # TODO PyMySQL
            exists = VLASSQl.objects.get(url=Link_CADCArchive)
            new_ql = exists
            print(f"Using already saved QL for {new_ql.subtile}!!!")

        except VLASSQl.DoesNotExist:
            # TODO PyMySQL
            new_ql = VLASSQl.create(header, image_data, Link_CADCArchive, optional_args)
            new_ql.save()

        # THIS HASN'T BEEN TESTED SINCE EDITED!!!!
        # also create region
        # TODO PyMySQL
        region = RegionQl.objects.filter(ql=new_ql)
        if not region:
            print("create new region")
            # TODO PyMySQL
            region = RegionQl.create(header, image_data, ql=new_ql)
            region.save()
        else:
            region = region[0]
        return new_ql, region

    # df is pybdsf dataframe after formatting
    @classmethod
    def create_from_pybdsf_output(cls, header, image_data, Link_CADCArchive, df):
        optional_args = {'Mean_isl_rms': float(np.mean(df['Isl_rms'])*1000.0),
                        'SD_isl_rms': float(np.std(df['Isl_rms'])*1000.0),
                        'Peak_flux_p25': float(np.quantile(df['Peak_flux'],0.25)*1000.0),
                        'Peak_flux_p50':float(np.quantile(df['Peak_flux'],0.50)*1000.0),
                        'Peak_flux_p75':float(np.quantile(df['Peak_flux'],0.75)*1000.0),
                        'Peak_flux_max':float(np.max(df['Peak_flux'])*1000.0),
                        'N_pybdsf_components':df['RA'].count()
                        }
        #print(optional_args)
        return cls.create_ql_with_region(header=header, image_data=image_data, Link_CADCArchive=Link_CADCArchive, optional_args=optional_args)
