# config/settings for the Pybdsf output formatting on new data injest
import astropy.units as u

#rms processing settings (pixels)
BOXSIZE = 200
STEPSIZE = 50
#the frequency in Hz of the input image
FREQUENCY = 2.987741489322E+09

# potentiaL extra pybdsf columns
EXTRACOLS = [
    'Maj',
    'E_Maj',
    'E_Maj',
    'Min',
    'E_Min',
    'PA',
    'E_PA',
    'Isl_Total_flux',
    'E_Isl_Total_flux',
    'Isl_rms',
    'Isl_mean',
    'Resid_Isl_rms',
    'Resid_Isl_mean',
    'RA_max',
    'DEC_max',
    'E_RA_max',
    'E_DEC_max',
    'Xposn_max',
    'E_Xposn_max',
    'Yposn_max',
    'E_Yposn_max',
    'Maj_img_plane',
    'E_Maj_img_plane',
    'Min_img_plane',
    'E_Min_img_plane',
    'PA_img_plane', 'E_PA_img_plane','DC_Maj',
    'E_DC_Maj',
    'DC_Min',
    'E_DC_Min',
    'DC_PA',
    'E_DC_PA',
    'DC_Maj_img_plane',
    'E_DC_Maj_img_plane',
    'DC_Min_img_plane',
    'E_DC_Min_img_plane',
    'DC_PA_img_plane',
    'E_DC_PA_img_plane',
    'NN_dist',
    'NVSS_distance',
    'FIRST_distance',
    'Peak_to_ring',
    'Duplicate_flag',
    'Component_name',
    'Source_type'
    ]

#= (r*u.deg).to(u.arcsec).value
UNITS = {
    'Total_flux' :
        {'old': u.Jy,
        'new': u.mJy} ,
    'E_Total_flux':
        {'old': u.Jy,
        'new': u.mJy},
    'Peak_flux':
        {'old': u.Jy/u.beam ,
        'new': u.mJy/u.beam} ,
    'E_Peak_flux':
        {'old': u.Jy/u.beam ,
        'new': u.mJy/u.beam} ,
    'Maj':
        {'old': u.deg,
        'new': u.arcsec},
    'E_Maj':
        {'old': u.deg,
        'new': u.arcsec},
    'Min':
        {'old': u.deg,
        'new': u.arcsec},
    'E_Min':
        {'old': u.deg,
        'new': u.arcsec},
    'Isl_Total_flux':
        {'old': u.Jy,
        'new': u.mJy},
    'E_Isl_Total_flux':
        {'old': u.Jy,
        'new': u.mJy},
    'Isl_rms':
        {'old': u.Jy/u.beam ,
        'new': u.mJy/u.beam} ,
    'Isl_mean':
        {'old': u.Jy/u.beam ,
        'new': u.mJy/u.beam} ,
    'Resid_Isl_rms':
          {'old': u.Jy/u.beam ,
          'new': u.mJy/u.beam} ,
    'Resid_Isl_mean':
        {'old': u.Jy/u.beam ,
        'new': u.mJy/u.beam} ,
    'Maj_img_plane':
        {'old': u.deg,
        'new': u.arcsec},
    'E_Maj_img_plane':
        {'old': u.deg,
        'new': u.arcsec},
    'Min_img_plane':
        {'old': u.deg,
        'new': u.arcsec},
    'E_Min_img_plane':
        {'old': u.deg,
        'new': u.arcsec},
    'DC_Maj':
        {'old': u.deg,
        'new': u.arcsec},
    'E_DC_Maj':
        {'old': u.deg,
        'new': u.arcsec},
    'DC_Min':
        {'old': u.deg,
        'new': u.arcsec},
    'E_DC_Min':
        {'old': u.deg,
        'new': u.arcsec},
    'DC_Maj_img_plane':
        {'old': u.deg,
        'new': u.arcsec},
    'E_DC_Maj_img_plane':
        {'old': u.deg,
        'new': u.arcsec},
    'DC_Min_img_plane':
        {'old': u.deg,
        'new': u.arcsec},
    'E_DC_Min_img_plane':
        {'old': u.deg,
        'new': u.arcsec}
}
