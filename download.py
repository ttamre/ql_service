import argparse
import bdsf
import logging
import requests
import time
import tempfile
import io
import os
import urllib3
from astropy.io import fits
from config import BOXSIZE, STEPSIZE, FREQUENCY
from processtools import format_custom_data, cleanup_csv_file_header, format_subtile_data


FAILED_DOWNLOAD_LOG = "logs/failed_ql_download.txt"
logging.basicConfig(level=os.environ.get("LOGLEVEL", "INFO"))
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)


"""
Process command line arguments and return a dictionary containing them

Parameters:
    default_attempts    (optional)  default number of request attempts per URL
    default_timeout     (optional)  default timeout for each request (in seconds)
    default_delay       (optional)  default delay between request attempts (in seconds)
Returns:
    args                a Namespace object of arguments (accessed like args.attempts)
"""
def process_arguments(default_attempts=3, default_timeout=45, default_delay=2):
    # Process command line arguments
    parser = argparse.ArgumentParser(prog="QL Service",
                                     description="Downloads QL images and processes them for use by the transient marshal")
    
    # Required arguments
    parser.add_argument("FILE", help="URL or FILE containing a list of URLs to QL images", type=str)

    # Optional arguments
    parser.add_argument("-a", "--attempts",
                        help="Number of attempts that will be made before aborting (default 3)",
                        nargs=1, required=False, default=default_attempts, type=int)
    parser.add_argument("-t", "--timeout",
                        help="Max waiting time for a single request (in seconds; default 45s)",
                        nargs=1, required=False, default=default_timeout, type=int)
    parser.add_argument("-d", "--delay",
                        help="Delay between attempts (in seconds; default 2s)",
                        nargs=1, required=False, default=default_delay, type=int)
    
    return parser.parse_args()


"""
Send a request to a given url
Upon failure, attempts the request a given number of times

Parameters:
    url         (required)  URL to send requests to
    attempts    (required)  Number of times the request will be reattempted (in seconds)
    timeout     (required)  Max timeout for a single request (in seconds)
    delay       (required)  Delay between attempts (in seconds)
Returns:
    response_data           Bytearray of the response from {url}  
"""
def send_request(url, attempts, timeout, delay):

        # Make {attempts} request attempts to {url}
        while attempts > 0:
            try:
                # response = urllib.request.urlopen(request)
                logging.info(f"REQUEST : {url}")
                response = requests.get(url, verify=False, timeout=timeout)
            except Exception as e:
                logging.warning(f"Exception getting fits for {url}: {str(e)}")

            else:
                try:
                    response_data = bytearray(response.content)
                    logging.info(f"RESPONSE: {response.content}")
                    return response_data
                except Exception as e:
                    print(f"{e}",is_traceback=True)

            attempts -= 1
            if attempts > 0:
                logging.info(f"Taking a {delay}s nap...")
                time.sleep(delay)
                logging.info("OK, lest trying fetching the FITS -- again!")

        # If nothing can be fetched after {attempts} attempts, log errors and raise exception
        logging.exception(f"Bailed on fetch for url: {url}")
        with open(FAILED_DOWNLOAD_LOG, "a") as logfile:
            logfile.write(f"{time.ctime(time.time())} - Bailed on fetch for url: {url}\n")

        raise Exception("Connection issue or Timeout retrieving FITS")


"""
Fetch a fits file from a given URL for processing, then save as CSV file

Parameters:
    ql_url  (required)  URL to send requests to
    args    (required)  Namespace object containing parameters for our request
Returns:
    dataframe           Returns a pandas.dataframe object containing our processed data
"""
def download_ql_image(args):
        # Convert URL or URLs into a list of URLs for easy processing
        try: # Attempt to read file into a list of URLs
             with open(args.FILE) as f:
                  urls = f.readlines()
        except: # If no file found, treat as a single URL
             urls = [args.FILE]

        # Generate temporary FITS and CSV files
        temp_fits = tempfile.NamedTemporaryFile(delete=False, suffix='.fits')
        temp_csv = tempfile.NamedTemporaryFile(delete=False, suffix='.csv')
        temp_fits_name = temp_fits.name
        temp_csv_name = temp_csv.name
        
        # Create QL file(s) based on the response from CADC
        dataframes = []
        for url in urls:
            fits_bytes = io.BytesIO(send_request(url, args.attempts, args.timeout, args.delay))
            fits_bytes.seek(0)
            return
            hdul = fits.open(fits_bytes, ignore_missing_end= True)
            hdul.writeto(temp_fits_name, overwrite=True)

            # Process QL image with PyBDSF and write response to the temp CSV file
            img = bdsf.process_image(input=temp_fits_name, rms_box=(BOXSIZE, STEPSIZE), frequency=FREQUENCY)
            img.write_catalog(outfile=temp_csv_name, format="csv", catalog_type= 'srl', clobber=True)

            # Data cleanup/formatting
            cleanup_csv_file_header(temp_csv_name)
            df = format_custom_data(pyout=temp_csv_name, ql_filename=temp_fits_name)
            dataframes.append(df)

        # TODO send files and delete temp files
        return dataframes


if __name__ == "__main__":
        test_url = "https://www.cadc-ccda.hia-iha.nrc-cnrc.gc.ca/data/pub/VLASS/VLASS1.1.ql.T01t01.J000228-363000.10.2048.v1.I.iter1.image.pbcor.tt0.subim.fits"
        args = process_arguments()
        args.FILE = test_url
        df = download_ql_image(args)
        print(df)