import re, os, tempfile, time, requests, io
import bdsf
import pandas as pd
import re
#from django.db import models
from astropy.io import fits
from django.utils import timezone
#from django.conf import settings
from astroquery.cadc import Cadc
from ql import QlManager, VLASSQl
from processtools import format_custom_data, cleanup_csv_file_header, format_subtile_data
from config import BOXSIZE, STEPSIZE, FREQUENCY
from ql import Processing, SkippedWithError, VLASSQl #,TemporalComponentSet, ComponentAttributes, Candidate
#from .cutoutfetcher import CutoutFetcher
# from .updates import Updates
from datetime import timedelta
from astropy.time import Time

#run test with
#from backend.transientmarshal.newdata import *
# NewData.get_and_insert_ql()
#for getting newly released VLASS QL images and adding to process queue
class NewData:
    def __init__(self):
        self.cadc = Cadc()
        self.all_rows = self.find_new_images()

    # row is 2020-06-26T23:27:31.000 ivo://cadc.nrc.ca/VLASS?VLASS2.1.T26t10.J131707+613000/VLASS2.1.T26t10.J131707+613000.quicklook
    @staticmethod
    def get_epoch_from_rowdata(row):
        return str(row['publisherID']).split("/")[-1].split('VLASS')[1][:3]

    @staticmethod
    def get_subtile_from_rowdata(row):
        ql =str(row['publisherID']).split("/")[-1]
        return re.search('J(.+?(?=\.))', ql).group(0)

    @staticmethod
    def check_ql_exists(row):
        subtile = NewData.get_subtile_from_rowdata(row)
        epoch = NewData.get_epoch_from_rowdata(row)
        if len(VLASSQl.objects.filter(epoch=epoch,subtile=subtile))>0:
            return True
        else:
            return False

    @staticmethod
    def check_bad_ql(row):
        flag = row['requirements_flag']
        return 'fail' in flag

    @staticmethod
    def ql_skipped_before(row):
        item = row['publisherID'].decode("utf-8").split("/")[-1].replace("T", "ql.T").replace(".quicklook","")
        return SkippedWithError.objects.filter(url__contains=item, success_retry=False).count()>0

    # failed QC:
    #all_rows = cadc.exec_sync(f"Select Plane.dataRelease, Plane.publisherID, Plane.time_bounds_lower, Plane.time_bounds_upper FROM \
    #caom2.Plane AS Plane JOIN caom2.Observation AS Observation ON Plane.obsID = Observation.obsID where Observation.collection = 'VLASS' \
    #AND Observation.requirements_flag = 'fail'  ORDER BY Plane.time_bounds_lower")


    def find_new_images(self):
        # 1) Get 
        latest = VLASSQl.objects.all().order_by('obs_date', 'created_on').last()
        print("\n\n\nlatest", latest, latest.obs_date)
        # after_date_string = str(latest.obs_date)
        # all_rows = self.cadc.exec_sync(f"Select Plane.dataRelease, Plane.publisherID FROM \
        #                         caom2.Plane AS Plane JOIN caom2.Observation AS Observation \
        #                         ON Plane.obsID = Observation.obsID where Observation.collection = 'VLASS' \
        #                         AND Plane.dataRelease >= '{after_date_string}' ORDER BY Plane.dataRelease")
        mjd_time = Time(latest.obs_date).mjd
        all_rows = self.cadc.exec_sync(f"Select Plane.dataRelease, Plane.publisherID, Plane.time_bounds_lower, \
                                    Plane.time_bounds_upper, Observation.requirements_flag FROM caom2.Plane AS Plane JOIN caom2.Observation AS Observation \
                                    ON Plane.obsID = Observation.obsID where Observation.collection = 'VLASS' \
                                    AND Plane.time_bounds_lower >= '{mjd_time}' ORDER BY Plane.time_bounds_lower, Plane.time_bounds_upper")


        print(len(all_rows), "New to process!\n\n\n")
        return all_rows

    #MAIN, gets and inserts one new image at a time for ongoing
    def get_and_insert_ql(self):
        pobj = None
        errs = ""
        if len(self.all_rows)==0:
            pobj = -1
            # no new images wait a day? and retry
            return pobj, errs
            # delay and try again after a while???
        #get image urls
        first = 0
        max_retries = len(self.all_rows)
        in_db = True
        should_skip = True
        while (in_db or should_skip) and first<max_retries:
            in_db = NewData.check_ql_exists(self.all_rows[first])
            should_skip = NewData.ql_skipped_before(self.all_rows[first])
            bad_quality = NewData.check_bad_ql(self.all_rows[first])
            if in_db or should_skip or bad_quality:
                print(f"\nexists already:{in_db}, should_skip:{should_skip}, bad_qualityQL:{bad_quality}, {self.all_rows[first][1]}")
                errs+=f"\nQL exists already:{in_db}, should_skip:{should_skip}, bad_qualityQL:{bad_quality}, {self.all_rows[first][1]}"
                if bad_quality: #log quality QL issue
                    url = self.cadc.get_data_urls(self.all_rows[first:first+1])[0]
                    data_release = self.all_rows[first][0]
                    bad_imgmsg = f"Bad QL Skipped with Quality flag: {self.all_rows[first]['requirements_flag']}"
                    SkippedWithError.objects.create(url=url, data_release=data_release, error_message=bad_imgmsg)
                    print(bad_imgmsg)
                first+=1
        if first>=max_retries:
            errs += "no more images left to try! \n"
            return pobj, errs
        print("try for this one!", self.all_rows[first][1])
        errs+=f"\ntry for this one! {self.all_rows[first][1]}"
        url = self.cadc.get_data_urls(self.all_rows[first:first+1])[0]
        data_release = self.all_rows[first][0]

        try:
            pobj = NewData.insert_new_image(ql_url=url)
        except Exception as e:
            print("EXCEPTION!!!\n\n\n", str(e))
            errs += str(e)
            try:
                SkippedWithError.objects.create(url=url, data_release=data_release, error_message=str(e))
            except Exception as e:
                errs += "\nerror skipped witherror creation: " + str(e)
                print(e)
        return pobj, errs

    # CHECK IF QL EXISTS BEFORE CALLING THIS!!!!
    @classmethod
    def insert_new_image(cls, ql_url):
        #test url
        #url = ["https://www.cadc-ccda.hia-iha.nrc-cnrc.gc.ca/data/pub/VLASS/VLASS1.1.ql.T01t01.J000228-363000.10.2048.v1.I.iter1.image.pbcor.tt0.subim.fits"]
        #download images and insert into db
        #for ql in url:

        # Generate temporary QL and PyBDSF files
        temp_ql = tempfile.NamedTemporaryFile(delete=False, suffix='.fits')
        temp_pybdsfout = tempfile.NamedTemporaryFile(delete=False, suffix='.csv')
        ql_filename = temp_ql.name
        pyout_csv = temp_pybdsfout.name
        
        pobj = None
        try:
            # Creates a QL files based on response from ql_url
            fits_file = io.BytesIO(Processing.send_request(ql_url))
            fits_file.seek(0)
            hdul = fits.open(fits_file, ignore_missing_end= True)
            hdul.writeto(ql_filename, overwrite=True)

            # Process QL image with PyBDSF
            img = bdsf.process_image(input=ql_filename, rms_box=(BOXSIZE, STEPSIZE), frequency=FREQUENCY)
            # header = hdul[0].header
            # image_data = hdul[0].data

            # Write processed image to the temp CSV file we created
            img.write_catalog(outfile=pyout_csv, format="csv", catalog_type= 'srl', clobber=True)

            # Data cleanup/formatting
            cleanup_csv_file_header(pyout_csv)
            semi_final_dataframe = format_custom_data(pyout = pyout_csv, ql_filename=ql_filename)
            return semi_final_dataframe

            # END OF PARALLELIZATION

            new_ql, region = QlManager.create_from_pybdsf_output(header=header, image_data=image_data, Link_CADCArchive=ql_url, df=semi_final_dataframe)
            #remove temp files
            os.remove(ql_filename)
            os.remove(pyout_csv)
            os.remove(ql_filename+".pybdsf.log")
            final_dataframe = format_subtile_data(df=semi_final_dataframe, region=region)
            # then create processing entry to go with QL
            pobj = Processing.save_new(new_ql, final_dataframe)
        except Exception as e:
            #cleanup
            try:
                os.remove(ql_filename)
            except Exception as e1:
                pass
            try:
                os.remove(pyout_csv)
            except Exception as e2:
                pass
            # catch this where it's called form and make skipped with error entry
            raise Exception(str(e))
        return pobj

if __name__ == "__main__":
    url = "https://www.cadc-ccda.hia-iha.nrc-cnrc.gc.ca/data/pub/VLASS/VLASS1.1.ql.T01t01.J000228-363000.10.2048.v1.I.iter1.image.pbcor.tt0.subim.fits"
    obj = NewData.insert_new_image(ql_url=url)
    print(obj)