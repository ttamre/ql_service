import re, tempfile, os
import numpy as np
import pandas as pd
from astropy.wcs import WCS
from django.conf import settings
from astropy.table import QTable
from astropy.coordinates import SkyCoord
from astropy import coordinates as coords
from astropy.io import fits
from PyAstronomy import pyasl
import astropy.units as u
from photutils import SkyCircularAperture
from photutils import SkyCircularAnnulus
from config import UNITS

def truncate_string_two_decimals(string):
    for to_fix in re.findall("\.[0-9]{3,5}", string):
        string = re.sub(to_fix, to_fix[:3], string)
    return string


def get_sexagesimal_string(position):
    # old way (updated July 21)
    # sexagesimal = "J" + re.sub(r'[h,m,s,d,\s]', '', position.to_string(style='hmsdms')) # remove letters
    # return truncate_string_two_decimals(sexagesimal)
    sexa = pyasl.coordsDegToSexa(position.ra.value, position.dec.value).replace(" ", "")
    sexcoords = sexa.split('+')
    sign = '+'
    if len(sexcoords)==1:
        sexcoords = sexa.split('-')
        sign='-'
    truncated = sexcoords[0][:9]+sign+sexcoords[1][:8] # agreed upon cirada sig dig for name: J033200.00+050000.0
    return "J" + truncated

#call this AFTER FORMATTING HEADER
def convert_column_units(df):
    py_cols = df.columns.tolist()
    for col in set(py_cols).intersection(list(UNITS)):
        df[col] = (df[col].map(lambda a: a*UNITS[col]['old'])).map(lambda x: x.to(UNITS[col]['new']).value)
    return df
    #= (r*u.deg).to(u.arcsec).value

def cleanup_csv_file_header(csv_file):
    if os.path.isfile(csv_file):
        with open(csv_file,"r+") as f:
            file_contents = list()
            is_keep = False
            for line in f:
                if re.match(r"^\# Source_id",line):
                    line =  re.sub(r"^# ","",line)
                    is_keep = True
                line = re.sub(r" +","",line)
                if is_keep:
                    file_contents.append(line)
            f.seek(0)
            f.truncate(0)
            for line in file_contents:
                f.write(line)
            f.close()

def tile_name(pybdsf_output_csv_filename):
    tile_name = re.sub(r"^.*?(T\d\dt\d\d).*$",r"\1",pybdsf_output_csv_filename)
    return tile_name

def subtile_name(pybdsf_output_csv_filename):
    subtile_name = re.sub(r"^.*?(J.*?)\..*$",r"\1",pybdsf_output_csv_filename)
    return subtile_name

# def first_distance(pybdsf_output):
#     # load first table
#     #first = QTable.read(surveys['FIRST']['file'])[ra,dec]
#     first = QTable.read(settings.FIRST_CAT)['RA','DEC']
#     df = pd.read_csv(pybdsf_output)
#     first_coords = SkyCoord(first['RA'].value,first['DEC'].value,unit='deg')
#     # get py coords
#     py_coords  = SkyCoord(df['RA'], df['DEC'], unit='deg')
#     # match coords
#     matched = py_coords.match_to_catalog_sky(first_coords)
#     return matched[1]
#
#
# def nvss_distance(pybdsf_output):
#     nvss = QTable.read(settings.NVSS_CAT)['RA(2000)','DEC(2000)']
#     # load table
#     df = pd.read_csv(pybdsf_output)
#     nvss_coords = SkyCoord(nvss['RA'].value,nvss['DEC'].value,unit='deg')
#     # get py coords
#     py_coords  = SkyCoord(df['RA'], df['DEC'], unit='deg')
#     # match coords
#     matched = py_coords.match_to_catalog_sky(nvss_coords)
#     return matched[1]


# adapted from: https://gitlab.com/cirada/continuum_bdp_catalogue_generator/-/blob/master/media/modules/vlad.py
def peak_to_ring(df, ql_image_file):
    # defaults
    r_units = u.arcsec
    r_inner = 5
    r_outer = 10

    # fh = fitsImageFileHandle(temp_fits.name)
    # open fits and format 2D
    fh = fits.open(ql_image_file)[0]
    fh_wcs = WCS(fh.header)
    naxis = fh_wcs.naxis
    while naxis > 2:
        fh_wcs = fh_wcs.dropaxis(2)
        naxis -= 1
    fh_data = np.squeeze(fh.data)

    positions = SkyCoord(df['RA'],df['DEC'], unit='deg')
    # get the inner circle (core) and outter annulus (ring)
    r_core = (r_inner * r_units).to(u.deg)
    r_ring = (r_outer * r_units).to(u.deg)

    a_core = SkyCircularAperture(positions,r_core).to_pixel(fh_wcs)
    a_ring = SkyCircularAnnulus(positions,r_core,r_ring).to_pixel(fh_wcs)

    # get their peak values
    pv_core = np.array([np.max(v.multiply(fh_data)) for v in a_core.to_mask(method='center')])
    pv_ring = np.array([np.max(v.multiply(fh_data)) for v in a_ring.to_mask(method='center')])

    # compute ratio
    # TO-DO: Handle nan's...
    peak_to_ring = pv_core/pv_ring
    #print(type(peak_to_ring))
    # try:
    #     os.remove(temp_fits.name)
    # except:
    #     print("no cleanup necessary")
    return peak_to_ring

# df is pytout table
def quality_flag(df, snmin=5, prmax=2, prdist=20):
    ###Q_flag
    ##3 fold:
    ## 1) Tot < Peak (GFIT)
    ## 2) Peak < 5*Isl_rms (SN)
    ## 3) dNN >= 20" && Peak_to_ring < 2 (PR)

    ###combine in to single flag value via binary bit addition
    #the quality flag is a sum of single flags, and everything starts at 0
    ## PR = 1; SN = 2; GFIT = 4
    ## weights GFIT highest, then SN, then P2R
    gfit, sn, pr = np.zeros(len(df)), np.zeros(len(df)), np.zeros(len(df))
    ###necessary column arrays
    stot = np.array(df['Total_flux'])
    speak = np.array(df['Peak_flux'])
    rms = np.array(df['Isl_rms'])
    dnn = np.array(df['NN_dist'])
    ptr = np.array(df['Peak_to_ring'])
    ###flag individual critera
    gfit[(speak > stot)] = 4
    sn[(speak < snmin*rms)] = 2
    pr[(dnn>=prdist) & (ptr<prmax)] = 1
    qflag = gfit + sn + pr
    #print("quality flag", qflag)
    return qflag

####nn dist needs to be limited to unique components that aren't empty islands
def nn_distance(py_df):
    ###create column for row number - allows easy remerge
    df = py_df.copy()
    df['dfix'] = df.index
    ###subset those to be used in the NN search (D_flag<2)
    ucomps = df[(df['Duplicate_flag']<2)].reset_index(drop=True)
    ##create sky position catalogue and self match to nearest OTHER component
    poscat = SkyCoord(ra=np.array(ucomps['RA']), dec=np.array(ucomps['DEC']), unit='deg')
    self_x = coords.match_coordinates_sky(poscat, poscat, nthneighbor=2)
    ###create new column in ucomps
    ucomps = ucomps.assign(NN_dist = self_x[1].arcsec)
    ###merge with df, fill na with -99
    df = pd.merge(df, ucomps[['dfix', 'NN_dist']], on='dfix', how='left')
    df['NN_dist'] = df['NN_dist'].fillna(-99)
    return df['NN_dist']

#NOTE OUR OWN VERSION ONLY LOOKS WITHIN SAME QL FOR DUPLICATES HERE since 2.1,2.2 data
def flag_duplicates(df, pos_err=2*u.arcsec):
    ###find duplicates and flag
    ###create SN column to sort by - may replace with q_flag later
    df['SN'] = df['Peak_flux']/df['Isl_rms']
    #2) sort by SN/qflag, subset dist<2"
    df = df.sort_values(by='SN', ascending=False).reset_index(drop=True)
    #####DONT subset duplicates!
    ###tun search around on entire catalogue (sorted) and use index you dumbass!
    dfpos = SkyCoord(ra=np.array(df['RA']), dec=np.array(df['DEC']), unit='deg')
    dsearch = dfpos.search_around_sky(dfpos, seplimit=pos_err)
    ###create dataframe for easy manipulation - not actually neccesary just cleaner
    dsdf = pd.DataFrame({'ix1': dsearch[0], 'ix2': dsearch[1], 'ix3': dsearch[2].arcsec})
    ###subset to ix1 != ix2 - reduces 4M to 500k
    dsdf = dsdf[(dsdf['ix1']!=dsdf['ix2'])].reset_index(drop=True)
    ###is index of preferred components where fist instance in ix1 occurs before ix2? - I think so
    ix1, ix2 = list(dsdf['ix1']), list(dsdf['ix2'])
    prefcomp = [i for i in ix1 if ix1.index(i) < ix2.index(i)] ##this takes a while
    ###use pref comp to filter dup array and reflag
    dupflag = np.zeros(len(df)) ##all set to zero
    if ix1:
        dupflag[np.unique(ix1)] = 2 ##flags all duplicates
    dupflag[prefcomp] = 1 ##reflags preferred duplicates
    return dupflag

def create_source_name(row):
    position = SkyCoord(ra=row['RA'], dec=row['DEC'], unit='deg')
    return get_sexagesimal_string(position)

def safe_coords(row):
    position = SkyCoord(ra=row['RA'], dec=row['DEC'], unit='deg')
    return [position.ra.value, position.dec.value]

def format_custom_data(pyout, ql_filename):
    print("format custom\n\n")
    df = pd.read_csv(pyout)
    df_updated = convert_column_units(df)
    df[['RA', 'DEC']] = df.apply(safe_coords, axis=1).tolist()
    #print("columns converted")
    df_updated['Peak_to_ring'] = peak_to_ring(df_updated, ql_filename)
    #print(df_updated['Peak_to_ring'])
    df_updated['Duplicate_flag'] = flag_duplicates(df_updated)
    #print("dups flagged")
    df_updated['NN_dist'] = nn_distance(df_updated)
    #print("nn done")
    df_updated['Quality_flag'] = quality_flag(df_updated)
    #print("quality flag done")
    df_updated['Source_name'] = df_updated.apply(create_source_name, axis=1)
    return df_updated

def format_subtile_data(df, region):
    df['Subtile'] = region.ql.subtile
    df[['Xposn', 'Yposn']] = df.apply(region.calculate_x_y, axis=1).tolist()
    df['E_Xposn'] = df['E_RA'].map(lambda x: abs(x/region.RA_pixelsize))
    df['E_Yposn'] = df['E_DEC'].map(lambda y: abs(y/region.DEC_pixelsize))
    return df
