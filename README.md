<div id="top"></div>



<!-- PROJECT LOGO -->
<br />
<div align="center">
  <a href="https://cirada.ca/">
    <img src="https://gitlab.com/cirada/website/-/raw/master/readme_images/cirada_logo.png" alt="Logo" height="100">
  </a>
  <h1 align="center">QL Service</h1>
  <br />
</div>

<p align="center">
  <img src="https://img.shields.io/badge/Ubuntu-18.04-lightgrey"/>
  <img src="https://img.shields.io/badge/Python-3.7.16-green"/>
</p>

## Installation

```bash
# Clone and enter repo
git clone https://gitlab.com/ttamre/ql_service.git
cd ql_service

# Create virtual environment and install dependencies
python -m venv venv
pip install -r requirements.txt
```

## Usage

``` bash
python download.py [URL or FILE] [...]

"""
REQUIRED ARGUMENTS (choose one)
  URL     TEXT    URL to a QL image
  FILE    TEXT    File containing a list of URLs to QL images

OPTIONAL ARGUMENTS
  -a  --attempts  INT   Number of attempts that will be made before aborting (default 3)
  -t  --timeout   INT   Max waiting time for a single request (in seconds; default 45s)
  -d  --delay     INT   Delay between attempts (in seconds; default 2s)
"""
```

Examples:

```bash
# Fetch/process a single file
python download.py "https://www.cadc-ccda.hia-iha.nrc-cnrc.gc.ca/data/pub/VLASS/VLASS1.1.ql.T01t01.J000228-363000.10.2048.v1.I.iter1.image.pbcor.tt0.subim.fits"

# Fetch/process multiple files (assuming ql_urls.txt is a text file with 1 URL per line)
python download.py ql_urls.txt

# Fetch/process multiple files with optional parameters
python download.py ql_urls.txt --attempts 1 --timeout 60 --delay 1
python download.py ql_urls.txt -a 1 -t 60 -d 1
```
